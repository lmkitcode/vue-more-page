import Vue from "vue";
import App from "./App.vue";
import api from "@utils/api";
import { get, post } from "@utils/request";

// import router from './router'
// import store from './store'

Vue.config.productionTip = false;
Vue.prototype.$Api = api;
Vue.prototype.$Post = post;
Vue.prototype.$Get = get;

new Vue({
  render: (h) => h(App),
}).$mount("#app");
