/*
 *@description: 配置页面存在的路径统一管理
 *@author: luomingkai
 *@date: 2022-01-11 20:55:24
 *@version: V1.0.0
*/
export const pathKeys = {
    PAGE_HOME: 'home-page.html', // 首页
    NOTICE_ANNOUNCEMENT: "notice-announcement.html", // 通知公告
    POLICY_PUBLICITY: "policy-publicity.html", // 政策公示
    RelevantHighlights:"relevant-highlights.html",//相关要文
    CHARGING_STANDARD: "charging-standard.html", // 收费标准
    EXAM_CENTER: 'examination-center.html', // 考试中心
    SCORE_QUERY: "score-query.html", // 成绩查询
    CERTIFICATE_QUERY: "certificate-query.html", // 证书查询
    ABOUT_US: "about-us.html", // 关于我们
    FIXED_BANNER_LINK: "https://fsyzp.bm001.com/course?nav=87", // 首页、通知公告、政策公示、收费标准固定图跳转链接
}


/*
 *@description: 导航跳转
 *@params1: path 页面路径
 *@params2: mode 方式 _self, _blank
 *@author: luomingkai
 *@date: 2022-01-08 15:03:26
 *@version: V1.0.0
*/
export const navJump = (path, mode = '_self') => {
    let reg = /^([hH][tT]{2}[pP]:\/\/|[hH][tT]{2}[pP][sS]:\/\/)(([A-Za-z0-9-~]+).)+([A-Za-z0-9-~\/])+$/;
    if (reg.test(path)) { // 网址链接
        window.open(path, mode);
    } else { // 内部链接
        let url = process.env.VUE_APP_WEB_PATH + path;
        window.open(url, mode);
    }
}

/*
 *@description: 获取当前链接参数
 *@params1: name 参数名称
 *@author: luomingkai
 *@date: 2022-01-12 15:03:26
 *@version: V1.0.0
*/
export const getQueryStringByUrl = (name) => {
    let url = window.location.href;
    let temp1 = url.split('?');
    if (temp1 && temp1.length > 1) {
        let pram = temp1[1];
        let keyValue = pram.split('&');
        let obj = {};
        for (var i = 0; i < keyValue.length; i++) {
            var item = keyValue[i].split('=');
            var key = item[0];
            var value = item[1];
            obj[key] = value;
        }
        return obj[name];
    } else {
        return "";
    }
}