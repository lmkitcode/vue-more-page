/**
 * @description 首先需要在definitionKeys配置key, 然后通过 StorageKeys[keyname] 获取的处理的key
 * 然后通StorageUtil {
 *    setSessionStorage,
 *    getSessionStorage,
 *    setLocalStorage,
 *    getLocalStorage
 * } 设置/获取
 * 列子：StorageUtil.setSessionStorage(StorageKeys.BASE_INFO, value)
 *       StorageUtil.getSessionStorage(StorageKeys.BASE_INFO)
 */

import secret from './secret.js'

// 配置key
const definitionKeys = {
    BASE_INFO: "base_info", // 基本配置信息
}

/**
 * 设置存储
 * @param {*} isSession 是否是会话存储
 * @param {*} key 存储key
 * @param {*} object 存储值
 * @param {*} expire 失效时间 （可选）
 * @returns 
 */
function setStorage (isSession, key, object, expire) {
    if (!key) {
        console.error("存储失败：请设置存储key");
        return;
    }
    try {
        let saveData = {
            data: object,
            endExpireTime: ""
        }
        if (!expire || Number.isNaN(Number(expire))) {
            if (expire != undefined) {
                console.info("存储警告：失效时间设置无效,数据存储正常存储但不会失效");
            }
        } else {
            let time = new Date().getTime() + (expire * 1000);
            saveData.endExpireTime = time || "";
        }
        let data = JSON.stringify(saveData);
        if (isSession) {
            sessionStorage.setItem(key, secret.Encrypt(data));
        } else {
            localStorage.setItem(key, secret.Encrypt(data));
        }
    } catch (error) {
        console.error(`存储失败：${error}`)
    }
}

/**
 * 获取存储值
 * @param {*} isSession 是否是获取会话存储
 * @param {*} key 存储key
 */
function getStorage (isSession, key) {
    console.log('获取key: ', key);
    if (!key) return "";
    let itemVal = {};
    if (isSession) {
        let ival = sessionStorage.getItem(key);
        itemVal = ival ? secret.Decrypt(ival) : '';
    } else {
        let ival = localStorage.getItem(key);
        itemVal = ival ? secret.Decrypt(ival) : '';
    }
    let data = "";
    if (itemVal) {
        itemVal = JSON.parse(itemVal);
        if (itemVal.endExpireTime) {
            let currentTime = new Date().getTime();
            if (itemVal.endExpireTime > currentTime) {
                data = itemVal.data;
            } else {
                if (isSession) {
                    sessionStorage.removeItem(key);
                } else {
                    sessionStorage.removeItem(key);
                }
                window.location.reload(); // 刷新
            }
        } else {
            data = itemVal.data;
        }
    }
    return data;
}

export const StorageUtil = {
    // 设置会话存储
    setSessionStorage: (key, object, expire) => {
        setStorage(true, key, object, expire)
    },

    // 获取会话存储
    getSessionStorage: (key) => {
        return getStorage(true, key)
    },

    // 设置本地存储
    setLocalStorage: (key, object, expire) => {
        setStorage(false, key, object, expire)
    },

    // 获取会话存储
    getLocalStorage: (key) => {
        return getStorage(false, key)
    },
}

export const StorageKeys = (() => {
    // 存储key的命名规则
    const prijectName = "zsxtwebpc";
    const env = process.env.NODE_ENV;
    const version = "1.0.0";
    let resKeys = {};
    for (const key in definitionKeys) {
        if (Object.hasOwnProperty.call(definitionKeys, key)) {
            const element = definitionKeys[key];
            resKeys[key] = `${prijectName}_${env}_${version}_${element}`
        }
    }
    return resKeys;
})();

