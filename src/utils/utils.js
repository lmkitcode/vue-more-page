/*
 *@params1: position 当前滚动位置
 *@params2: destination 目标位置
 *@params3: rate 缓动率
 *@params4: callback 缓动结束回调函数 两个参数分别是当前位置和是否结束
 *@description: 滚动条的缓冲函数
 *@author: luomingkai
 *@date: 2022-01-13 14:25:10
 *@version: V1.0.0
*/
export const easeout = (position, destination, rate, callback) => {
	if (position === destination || typeof destination !== 'number') {
		return false
	}
	destination = destination || 0
	rate = rate || 2

	// 不存在原生`requestAnimationFrame`，用`setTimeout`模拟替代
	if (!window.requestAnimationFrame) {
		window.requestAnimationFrame = function (fn) {
			return setTimeout(fn, 17)
		}
	}

	let step = function () {
		position = position + (destination - position) / rate
		if (Math.abs(destination - position) < 1) {
			callback(destination, true)
			return
		}
		callback(position, false)
		requestAnimationFrame(step)
	}
	step()
}
