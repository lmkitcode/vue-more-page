import axios from "axios";
// import QS from "qs";
import { message } from "element-ui";

axios.defaults.baseURL = process.env.VUE_APP_API_BASE_URL;
axios.defaults.timeout = 10000;
axios.defaults.headers.post["Content-Type"] = "application/json;charset=UTF-8";

// 请求拦截器
axios.interceptors.request.use(
  (config) => {
    // 每次发送请求之前判断vuex中是否存在token
    // 如果存在，则统一在http请求的header都加上token，这样后台根据token判断你的登录情况
    // 即使本地存在token，也有可能token是过期的，所以在响应拦截器中要对返回状态进行判断
    // const token = token;
    // token && (config.headers.Authorization = token);
    return config;
  },
  (error) => {
    return Promise.error(error);
  }
);

// 响应拦截器
axios.interceptors.response.use(
  (response) => {
    // 如果返回的状态码为200，说明接口请求成功，可以正常拿到数据
    // 否则的话抛出错误
    if (response.status === 200) {
      const { data, rescode, msg } = response.data;
      if (rescode === 200) {
        return Promise.resolve(data);
      } else {
        return Promise.reject({
            response: {
                status: 201,
                msg: msg
            }
        });
      }
    } else {
      return Promise.reject(response);
    }
  },
  (error) => {
    switch (error.response.status) {
      case 404: {
        message({
          showClose: true,
          message: "数据好像走丢了~",
          type: "error",
        });
        break;
      }
      case 201: {
        message({
          showClose: true,
          message: error.response.msg,
          type: "error",
        });
        break;
      }
      default: {
        message({
          showClose: true,
          message: "系统好像出问题了~",
          type: "error",
        });
        break;
      }
    }
    return Promise.reject(error.response);
  }
);

/*
 *@params1: url
 *@params2: params
 *@description: get请求
 *@author: luomingkai
 *@date: 2022-01-08 16:41:44
 *@version: V1.0.0
 */
export function get(url, params = {}) {
  return new Promise((resolve, reject) => {
    axios
      .get(url, {
        params: params,
      })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
}

/*
 *@params1: url
 *@params2: params
 *@description: post请求
 *@author: luomingkai
 *@date: 2022-01-08 16:41:44
 *@version: V1.0.0
 */
export function post(url, params={}) {
  return new Promise((resolve, reject) => {
    axios
      .post(url, params)
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
}
