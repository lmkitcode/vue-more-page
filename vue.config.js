const path = require('path')
const glob = require('glob')
const PAGES_PATH = './src/pages/*/*.js'
const SpeedMeasurePlugin = require('speed-measure-webpack-plugin')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const ParallelUglifyPlugin = require('webpack-parallel-uglify-plugin');
const CompressionPlugin = require("compression-webpack-plugin");
const smp = new SpeedMeasurePlugin()

const resolve = dir => path.join(__dirname, dir)

// 生成多页面配置
const generatePages = () => {
    let pages = {}
    glob.sync(PAGES_PATH).forEach(filepath => {
        let fileList = filepath.split('/')
        let fileName = fileList[fileList.length - 2]
        let htmlFileName = fileName.replace(/[A-Z]/g, '-$&').toLowerCase() // html文件名以中横线为主
        pages[fileName] = {
            entry: filepath,
            template: `public/index.html`, // 'public/index.html'
            filename: `${htmlFileName}.html`,
            chunks: ['chunk-vendors', 'chunk-common', fileName]
        }
    })
    return pages
}


module.exports = {
    publicPath: process.env.VUE_APP_WEB_PATH,
    productionSourceMap: false, //  是否为生产环境构建生成 source map？
    css: {
        sourceMap: false,
    },
    pages: generatePages(),
    chainWebpack: config => {
        config.resolve.alias
            .set('@', resolve('src'))
            .set('@components', resolve('src/components'))
            .set('@utils', resolve('src/utils'))
    },
    // configureWebpack: process.env.NODE_ENV === "production" ? smp.wrap({
    //     plugins: [
    //         new BundleAnalyzerPlugin(),
    //         new ParallelUglifyPlugin({
    //             cacheDir: '.cache/',
    //             uglifyJS: {
    //                 output: {
    //                     beautify: false,
    //                     comments: false
    //                 },
    //                 warnings: false,
    //                 compress: {
    //                     drop_console: true,
    //                     collapse_vars: false,
    //                     reduce_vars: false
    //                 }
    //             }
    //         }),
    //         new CompressionPlugin({
    //             test: /\.(js|html|css)$/,
    //             threshold: 1024,
    //             minRatio: 0.8
    //         })
    //     ]
    // }) : {},
    configureWebpack: {
        plugins: [
            // new BundleAnalyzerPlugin(),
            new ParallelUglifyPlugin({
                cacheDir: '.cache/',
                uglifyJS: {
                    output: {
                        beautify: false,
                        comments: false
                    },
                    warnings: false,
                    compress: {
                        drop_console: true,
                        collapse_vars: false,
                        reduce_vars: false
                    }
                }
            }),
            new CompressionPlugin({
                test: /\.(js|html|css)$/,
                threshold: 1024,
                minRatio: 0.8
            })
        ]
    }
}