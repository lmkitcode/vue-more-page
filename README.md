## 项目依赖安装

```
yarn install
```

### 项目启动

```
yarn serve
```

### 项目打包

```
yarn build
```

## 项目介绍

项目为 vue2 为基础构建的多页面的架构，页面生成主要集中在 pages 目录下创建模块
模块的基本结构
src
—— assets // 公用文件（不太建议存储东西，减少包体积）
—— components // 公用组件存放
—— pages
—— 模块名
—— components // 模块内部组件
—— router // 路由
—— store // 状态管理器
—— App.vue
—— main.js

#### index.js 路由模板

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
{
path: '/',
name: 'About',
// route level code-splitting
// this generates a separate chunk (about.[hash].js) for this route
// which is lazy-loaded when the route is visited.
component: () => import(/_ webpackChunkName: "about" _/ '../views/About.vue')
}
]

const router = new VueRouter({
mode: 'history',
base: process.env.BASE_URL,
routes
})

export default router
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

### index.js 状态管理器模板

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
state: {
},
mutations: {
},
actions: {
},
modules: {
}
})

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
